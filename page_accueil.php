<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="./images/quiz.png" />
    <link rel="stylesheet" href="./styles/reset.css">
    <link rel="stylesheet" href="./styles/page_accueil.css">
    <title>Home</title>
</head>
<body>
    <section class="box">
        <div class="top">
            <div class="logo">
                <img src="./images/logo.png" alt="logo">
            </div>
            <h1 class="title">
                Testez vos connaissances
            </h1>
        </div>
        <div class="svg-wrapper">
            <svg height="60" width="320" xmlns="http://www.w3.org/2000/svg">
                <rect class="shape" height="60" width="320" />
            </svg>
            <div>
                <a class="text" href="./qcm_workshop.php">
                    Commencer
                </a>
            </div>
        </div>
    </section>
</body>
</html>