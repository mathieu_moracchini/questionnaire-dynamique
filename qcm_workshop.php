<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="./images/quiz.png" />
    <link rel="stylesheet" href="./styles/reset.css">
    <link rel="stylesheet" href="./styles/qcm_workshop.css">
    <title>Questionnaire</title>
</head>
<body>
    <header class="top">
        <div class="logo">
            <img src="./images/logo.png" alt="logo">
        </div>
        <h1 class="title">
            Questionnaire 
        </h1>
    </header>
    <section class="center">
        <?php

        $dossier = fopen ("qcm.txt", "r"); 
        $u = 1; 

        while (FALSE !== ($ligne = fgets($dossier))){ 
            $tableau = explode("##",$ligne);
            
            echo("<form class=\"contain\" action=\"score_workshop.php\" method=\"post\">");
            echo("<tr>");
            echo("<td><span class=\"numero\">"."Question n-".$u."</span><span class=\"question\"> : ".$tableau[0]."</span></td>");
            //echo("<td><h3>".$tableau[0]."</h3></td>");
            $tableau = array_slice($tableau,1);

            echo("<div class=\"parentdiv\">");
            foreach($tableau as $valeur){
            $valeur = str_replace("(","",$valeur);
            $valeur = str_replace(")","",$valeur);
            echo("<div class=\"box\">");
            echo("<div class=\"red\"></div>");
            echo("<input class=\"box-click\" type=\"radio\" name=\"case".$u."\" value=\"$valeur\">".$valeur."</input><br>");
            echo("</div>");
        }
            $u++;
            echo("</div>");
            echo("<br><br>");
            echo("</tr>");

            // si u depasse une taille on arrete
            if($u > 15)
            { 
                break; 
            }
        }
            echo("<br><input class=\"button\" type=\"submit\">");
            echo("</form>");

        ?>
    </section>
    <footer class="bot">
        <div class="fb">
            <a href="https://fr-fr.facebook.com/">
                <img class="fb-img" src="./images/fb.png"" alt="facebook">
            </a>
        </div>
        <div class="insta">
            <a href="https://www.instagram.com/?hl=fr">
                <img class="insta-img" src="./images/insta.png" alt="">
            </a>
        </div>
        <div class="tw">
            <a href="https://twitter.com/?lang=fr">
                <img class="tw-img" src="./images/tw.png" alt="">
            </a>
        </div>
    </footer>
</body>
</html>