<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="./images/quiz.png" />
    <link rel="stylesheet" href="./styles/reset.css">
    <link rel="stylesheet" href="./styles/score_workshop.css">
    <title>Score</title>
</head>
<body>
    <header class="top">
        <div class="logo">
            <img src="./images/logo.png" alt="logo">
        </div>
        <h1 class="title">
            Score 
        </h1>
    </header>
    <section class="box">
        <?php

        $dossier = fopen ("qcm.txt", "r"); //ouvrir le fichier
        $n = 1;
        $score = 0;

        while (FALSE !== ($ligne = fgets($dossier))){ //cette ligne permet de revenir à la ligne apres avoir lu la ligne tant qu'il y a des lignes à lire.
            $tableau = explode("##",$ligne);
            $tableau = array_slice($tableau,1);
                foreach($tableau as $valeur){
                    $first = "(".$_POST["case".$n].")";
                    $second = $_POST["case".$n];
                    if($first==$valeur){
                        $score++;
                    }
                }
                $n++;
            }

        $pourcent = 15;
        $pourcentage = $score/$pourcent;
        $pourcentage = $pourcentage * 100;
        $pourcentage = 100 - $pourcentage;
        $pourcentage = round($pourcentage, 1);

        echo("<div class=\"score\">");
        echo("<p class=\"txt-score\">Votre score est de : <span>".$score."/15</span></p>.");
        echo("<br><p class=\"txt-pourcent\">Votre pourcentage d'erreur est de <strong>".$pourcentage."%</strong></p>.");
        echo("</div>");
        //

        function creer_fichier($lieu,$information)
        {
            $fichier = fopen($lieu, "w") or die("Erreur fopen"); //die tue le code et fait un ehco
            
            if (!fwrite($fichier, $information))
            {
                echo("Erreur fwrite"); 
            }

            fclose($fichier) or die("Erreur fclose");

            return(true); 
        }

        creer_fichier("texte.txt","Votre SCORE est de ".$score."/15.\nDe plus, vous avez ".$pourcentage."% de mauvaises réponses.\n \nMerci d'avoir participé à notre questionnaire. \nRetrouvez nous sur les réseaux sociaux pour des rencontres avec les développeurs. \n \nD'autres projets sont en cours.");
        echo("<div class=\"fichier\">");
        echo("<br><br><em>PS: Votre fichier contenant vos résultats a été créer !</em>");
        echo("</div>");

        ?>
    </section>
    <footer class="bot">
        <div class="button">
            <a class="link" href="./page_accueil.php">
                Recommencer
            </a>
        </div>
    </footer>
</body>
</html>